# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/ffcv/issues
# Bug-Submit: https://github.com/<user>/ffcv/issues/new
# Changelog: https://github.com/<user>/ffcv/blob/master/CHANGES
# Documentation: https://github.com/<user>/ffcv/wiki
# Repository-Browse: https://github.com/<user>/ffcv
# Repository: https://github.com/<user>/ffcv.git
